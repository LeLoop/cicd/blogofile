IMAGE_NAME ?= leloop/blogofile

.PHONY: build push

build:
	docker build -t $(IMAGE_NAME):latest -f Dockerfile .

push:
	docker push $(IMAGE_NAME):latest

