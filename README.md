# leloop/blogofile

## What

The required toolchain to update [Le Loop Hackerspace](http://leloop.org)
website:

* Debian 10.3
* Python 2.7
* Blogofile 0.7.1
* BeautifulSoup 3.3.2

## Why

The BeautifulSoup [3.x series is discontinued](https://pypi.org/project/BeautifulSoup/3.2.2/)
and the `beautifulsoup` pip package will be updated to the 4.x series, which
will break the build. Should we need to update the website (you never know),
putting this Docker stack together was quicker than porting the stack to
Python 3.

We think the same goes for our old websites as it does for our old folks. We
must provide them with a peaceful retirement. They will probably outlive us.

## How

The whole point being to freeze the toolchain over time, there should not
be much need to update this image. Should you need to, just use the provided  [Makefile](https://gitlab.com/LeLoop/cicd/blogofile/-/blob/master/Makefile).

Usage should looks like:

```sh
docker run --rm \
    --volume "$(pwd):/source" \
    leloop/blogofile blogofile build
```

## Where

* [Docker Hub](https://hub.docker.com/repository/docker/leloop/blogofile)
* [Source code](https://gitlab.com/leloop/cicd/blogofile)
* Our [website (outdated) documentation](http://wiki.leloop.org/index.php/Howto:Mise_%C3%A0_jour_du_site_web)
* This image [might be used for the CI](https://gitlab.com/LeLoop/www/le-loop-blog/-/blob/master/.gitlab-ci.yml).
